<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('admin')->group(function (){
    Auth::routes();

    Route::get('email', function (){
        return view('email');
    });

    Route::get('dashboard', 'HomeController@index')->name('dashboard');

    Route::get('calendar', 'CalendarController@index')->name('calendar');

    Route::resource('booking', 'BookingController');


    Route::get('rates/add/{id}', 'RatesController@create')->name('add_rates');
    Route::resource('rates', 'RatesController');

    Route::prefix('rooms')->group(function (){

        Route::get('room-types/create/picture', 'RoomTypePhotoController@index')->name('room_types_picture');
        Route::resource('room-types', 'RoomTypeController');
        Route::post('room-types/destroymany', 'RoomTypeController@destroyMany')->name('room_types_destroymany');

    });


    Route::get('rooms/add/{id}', 'RoomController@create')->name('add_room');
    Route::post('rooms/add/{id}', 'RoomController@store')->name('add_room_store');
    Route::resource('rooms', 'RoomController');

    Route::resource('payments', 'PaymentsController');

    Route::get('customers/search', 'CustomerController@search')->name('customers_search');
    Route:: resource('customers', 'CustomerController');
    Route::post('customers/destroymany', 'CustomerController@destroyMany')->name('customers_destroymany');

    Route::resource('pages', 'PagesController');


    Route::prefix('settings')->group(function (){
        Route::get('facilities/facility-type', 'FacilityController@setFacilityType')->name('facilities_typeset');
        Route::resource('facilities', 'FacilityController');
        Route::post('facilities/destroymany', 'FacilityController@destroyMany')->name('facilities_destroymany');

        Route::resource('facility-types', 'FacilityTypeController');
        Route::post('facility-types/destroymany', 'FacilityTypeController@destroyMany')->name('facility_types_destroymany');

        Route::resource('services', 'ServiceController');
        Route::post('services/destroymany', 'ServiceController@destroyMany')->name('services_destroymany');

        Route::post('policy/add-rule', 'PolicyController@saveRule')->name('partially_refund_rule');
        Route::resource('policy', 'PolicyController');
        Route::post('policy/destroymany', 'PolicyController@destroyMany')->name('policy_destroymany');
    });

    Route::prefix('system')->group(function (){
        Route::resource('users', 'UserController');
        Route::post('users/destroymany', 'UserController@destroyMany')->name('users_destroymany');

        Route::resource('roles', 'RoleController');
        Route::post('roles/destroymany', 'RoleController@destroyMany')->name('roles_destroymany');

        Route::resource('permissions', 'PermissionController');
        Route::post('permissions/destroymany', 'PermissionController@destroyMany')->name('permission_destroymany');

        Route::resource('statuses', 'StatusController');

        Route::resource('languages', 'LanguageController');
        Route::post('languages/destroymany', 'LanguageController@destroyMany')->name('language_destroymany');
        Route::get('languages/makedefault/{id}', 'LanguageController@makeDefault')->name('languages_makedefault');

    });



});
