@extends('layouts.admin')

@section('mini-menu')
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
                </a>
                <ul>
                    <li>
                        <a href="{{route('rooms.index')}}"><span class="title">Rooms</span></a>
                    </li>
                    <li class="active">
                        <a href="javascript:;">Room Types</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('rooms.index')}}">Rooms</a></li>
                <li class="breadcrumb-item active">Room Types</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <h1>ROOM TYPES
                    <a href="{{route('room-types.create')}}" class="btn btn-hotel btn-primary-hotel btn-top-page pull-right"><i class="icon-plus"></i> Add room type</a>

                </h1>


            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-block">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('flash_message') !!}
                    </div>

                @endif
                @if(Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('warning') !!}
                    </div>

                @endif
                @if(Session::has('danger'))
                    <div class="alert alert-danger" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('danger') !!}
                    </div>

                @endif

            </div>

            <div class="table-responsive">
                <div id="basicTable_wrapper" class="dataTables_wrapper no-footer">
                    <form action="{{route('room_types_destroymany')}}" method="post">
                        {{csrf_field()}}
                        <table class="table table-hover dataTable no-footer" id="basicTable" role="grid">
                            <thead>
                            <tr role="row">
                                <th style="width: 1%" class="sorting_desc" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1" aria-label="No.: activate to sort column ascending" aria-sort="descending">No</th>
                                <th style="width: 63%" class="sorting" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending">Name</th>
                                <th style="width: 20%" class="sorting" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
                                <th style="width: 15%" class="sorting" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1">Action</th>
                                <th style="width:1%" class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="">
                                    <button class="btn btn-link"><i class="icon-trash"></i>
                                    </button>
                                </th>
                            </tr>

                            </thead>
                            <tbody>
                            @if(sizeof($room_types) != 0)
                                @foreach ($room_types as $type)
                                    <tr role="row" class="odd">
                                        <td class="v-align-middle sorting_1">
                                            {{ $type->id }}
                                        </td>
                                        <td class="v-align-middle">
                                            {{ $type->translateDefault()->name }}
                                        </td>
                                        <td class="v-align-middle">
                                            {{ $type->status->name }}
                                        </td>
                                        <td class="v-align-middle">
                                            <a href="{{{route('room-types.edit', $type->id)}}}" class="btn btn-hotel btn-edit pull-left" style="margin-right: 3px;"><i class="icon-pencil"></i></a>
                                            <button type="button" class="btn btn-hotel btn-delete pull-left" style="margin-right: 3px;" data-target="#deleteModal{{$type->id}}" data-toggle="modal"><i class="icon-trash"></i></button>

                                        </td>
                                        <td class="v-align-middle">
                                            <div class="checkbox text-center">
                                                <input type="checkbox" value="{{$type->id}}" id="checkbox{{$type->id}}" name="multi_id[]">
                                                <label for="checkbox{{$type->id}}" class="no-padding no-margin"></label>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                            @else
                                <tr><td></td><td>No room types were found.</td><td></td><td></td><td></td></tr>

                            @endif




                            </tbody>
                            <tfoot>
                            <tr><td></td><td></td><td></td><td style="text-align: right">with selected:</td><td><button type="button" class="btn btn-xs btn-hotel btn-delete" data-target="#deleteMany" data-toggle="modal"><i class="icon-trash"></i> Delete</button></td>
                                <div class="modal fade fill-in disable-scroll" id="deleteMany" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header clearfix text-left">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                                </button>
                                            </div>
                                            <div class="modal-body text-center m-t-20">
                                                <h5>Are you sure you want to delete selected room types ?</h5>
                                                <p>If you delete selected room types, That policy will be lost in room</p>
                                                <button type="button" class="btn btn-hotel btn-primary-hotel" data-dismiss="modal" aria-hidden="true">Cancel</button>&nbsp;
                                                <button class="btn btn-hotel btn-delete" type="submit">Delete</button>

                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            </tr>



                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>


            <div style="text-align: center">
            </div>
        </div>
        <!-- END card -->


        <!-- DELETE MODAL -->
        @if(sizeof($room_types) > 0)
            @foreach ($room_types as $type)
                <div class="modal fade fill-in disable-scroll" id="deleteModal{{$type->id}}" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                </button>
                            </div>
                            <div class="modal-body text-center m-t-20">
                                <form action="{{ route('room-types.destroy', $type->id) }}" method="post">
                                    <input type="hidden" name="_method" value="delete" />
                                    {!! csrf_field() !!}
                                    <h5>Are you sure you want to delete {{ $type->translateDefault()->name }}?</h5>
                                    <p>If you delete {{$type->translateDefault()->name}}, This service will be lost in room</p>
                                    <button type="button" class="btn btn-hotel btn-primary-hotel" data-dismiss="modal" aria-hidden="true">Cancel</button>&nbsp;
                                    <button class="btn btn-hotel btn-delete" type="submit">Delete</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

        @endforeach
    @endif


    <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection

