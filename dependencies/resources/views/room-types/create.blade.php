@extends('layouts.admin')

@section('mini-menu')
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
                </a>
                <ul>
                    <li>
                        <a href="{{route('rooms.index')}}"><span class="title">Rooms</span></a>
                    </li>
                    <li class="active">
                        <a href="javascript:;">Room Types</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('rooms.index')}}">Rooms</a></li>
                <li class="breadcrumb-item"><a href="{{route('room-types.index')}}">Room Types</a></li>
                <li class="breadcrumb-item active">Add Room Type</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <h1>ADD ROOM TYPE
                </h1>

            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="card card-transparent">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('flash_message'))
                            <div class="alert alert-success" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                {!! Session('flash_message') !!}
                            </div>

                        @endif

                        @if (sizeof($errors) != 0)
                            <div class="alert alert-danger" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                @foreach($languages as $language)
                                    @if($errors->has('name'.$language->abbreviation)){{ $errors->first('name'.$language->abbreviation) }}<br>@endif
                                    @if($errors->has('description'.$language->abbreviation)){{ $errors->first('description'.$language->abbreviation) }}<br>@endif
                                @endforeach
                                @if($errors->has('status')){{ $errors->first('status') }}<br>@endif
                                @if($errors->has('area')){{ $errors->first('area') }}<br>@endif
                                @if($errors->has('bedtype')){{ $errors->first('bedtype') }}<br>@endif
                                @if($errors->has('max_adult')){{ $errors->first('max_adult') }}<br>@endif
                                @if($errors->has('max_child')){{ $errors->first('max_child') }}<br>@endif
                                @if($errors->has('bathroom')){{ $errors->first('bathroom') }}<br>@endif
                                @if($errors->has('facilities')){{ $errors->first('facilities') }}<br>@endif
                                @if($errors->has('ordering')){{ $errors->first('ordering') }}<br>@endif

                            </div>


                        @endif
                        <p class="small hint-text">* Indicates a required field.</p>
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off" action="{{route('room-types.store')}}" method="post" novalidate="novalidate">
                            {{csrf_field()}}
                            <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Name *</label>
                                <div class="col-md-9">
                                    <div class="card card-transparent">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-tabs-linetriangle hidden-sm-down" data-init-reponsive-tabs="dropdownfx">
                                            @foreach($languages as $language)
                                                @if($language->default == 1)
                                                    <li class="nav-item">
                                                        <a href="#" class="active" data-toggle="tab" data-target="#{{$language->abbreviation}}" aria-expanded="true"><span>{{$language->name}}</span></a>
                                                    </li>
                                                @else
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#{{$language->abbreviation}}" class="" aria-expanded="false"><span>{{$language->name}}</span></a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            @foreach($languages as $language)
                                                @if($language->default == 1)
                                                    <div class="tab-pane fade active show" id="{{$language->abbreviation}}" aria-expanded="true">
                                                        <input type="text" class="form-control error" id="name{{$language->abbreviation}}" placeholder="Suite" name="name{{$language->abbreviation}}"  value="{{old('name'.$language->abbreviation)}}" required="" aria-required="true" aria-invalid="true">

                                                    </div>
                                                @else
                                                    <div class="tab-pane" id="{{$language->abbreviation}}" aria-expanded="true">
                                                        <input type="text" class="form-control error" id="name{{$language->abbreviation}}" placeholder="Suite" name="name{{$language->abbreviation}}"  value="{{old('name'.$language->abbreviation)}}" required="" aria-required="true" aria-invalid="true">

                                                    </div>
                                                @endif
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Description *</label>
                                <div class="col-md-9">
                                    <div class="card card-transparent">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-tabs-linetriangle hidden-sm-down" data-init-reponsive-tabs="dropdownfx">
                                            @foreach($languages as $language)
                                                @if($language->default == 1)
                                                    <li class="nav-item">
                                                        <a href="#" class="active" data-toggle="tab" data-target="#des{{$language->abbreviation}}" aria-expanded="true"><span>{{$language->name}}</span></a>
                                                    </li>
                                                @else
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#des{{$language->abbreviation}}" class="" aria-expanded="false"><span>{{$language->name}}</span></a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            @foreach($languages as $language)
                                                @if($language->default == 1)
                                                    <div class="tab-pane fade active show" id="des{{$language->abbreviation}}" aria-expanded="true">
                                                        <textarea class="form-control error" id="description{{$language->abbreviation}}" name="description{{$language->abbreviation}}"  required="" aria-required="true" aria-invalid="true">
                                                        {{old('description'.$language->abbreviation)}}
                                                        </textarea>
                                                    </div>
                                                @else
                                                    <div class="tab-pane" id="des{{$language->abbreviation}}" aria-expanded="true">
                                                        <textarea class="form-control error" id="description{{$language->abbreviation}}" name="description{{$language->abbreviation}}"  required="" aria-required="true" aria-invalid="true">
                                                        {{old('description'.$language->abbreviation)}}
                                                        </textarea>
                                                    </div>
                                                @endif
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('area') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Area *</label>
                                <div class="col-md-9">
                                    <div class="input-group date col-md-12 p-l-0">
                                        <input type="text" class="form-control" name="area " placeholder="0.00"><span class="input-group-addon">SQ.M</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('bedtype') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Bed Type *</label>
                                <div class="col-md-9">
                                   <select class="form-control" name="bedtype" id="bedtype">
                                       @foreach($bed as $bed)
                                       <option value="{{$bed->id}}">{{$bed->translateDefault()->name}}</option>
                                           @endforeach
                                   </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('bathroom') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Bathroom *</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="bathroom" id="bathroom">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('max_adult') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Max Adult *</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="max_adult" id="max_adult">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('max_child') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Max Child *</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="max_child" id="max_child">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('facilities') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Facility *</label>
                                <div class="col-md-9">
                                    <select class=" full-width" data-init-plugin="select2" multiple name="facilities[]">
                                        @foreach($facilities as $facility)
                                            <option value="{{$facility->id}}">{{$facility->translateDefault()->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('ordering') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Ordering *</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="ordering" id="ordering">
                                        <option value="">Non Set</option>
                                        @for($i = 1 ; $i<=$order ; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Default Rate *</label>
                                <div class="col-md-9">
                                    <div class="input-group date col-md-12 p-l-0">
                                        <input type="text" class="form-control" name="price" placeholder="0.00"><span class="input-group-addon">Baht</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-3 control-label">Status *</label>
                                <div class="col-md-9">
                                    @foreach($status as $status)
                                        @if(old('status') == $status->id)
                                            {{ Form::radio('status',  $status->id, true) }}
                                            {{ Form::label($status->name, ucfirst($status->name)) }}<br>
                                        @else
                                            {{ Form::radio('status',  $status->id ) }}
                                            {{ Form::label($status->name, ucfirst($status->name)) }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>


                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <a href="{{route('room-types.index')}}" class="btn btn-hotel btn-primary-hotel">Back</a>
                                    <button class="btn btn-hotel btn-sec-hotel" type="submit">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
@endsection




