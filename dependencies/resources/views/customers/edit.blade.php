@extends('layouts.admin')

@section('mini-menu')
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('customers.index')}}">Customers</a></li>
                <li class="breadcrumb-item active">Edit {{$customer->firstname." ".$customer->lastname}}</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <h1>EDIT {{strtoupper($customer->firstname)." ".strtoupper($customer->lastname)}}

                </h1>

            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="card card-transparent">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('flash_message'))
                            <div class="alert alert-success" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                {!! Session('flash_message') !!}
                            </div>

                        @endif
                        @if ($errors->has('firstname') or $errors->has('lastname') or $errors->has('address') or $errors->has('city') or $errors->has('country') or $errors->has('postcode') or $errors->has('email') or $errors->has('phone'))
                            <div class="alert alert-danger" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                @if($errors->has('firstname')){{ $errors->first('firstname') }}<br> @endif
                                @if($errors->has('lastname')){{ $errors->first('lastname') }}<br> @endif
                                @if($errors->has('address')){{ $errors->first('address') }}<br> @endif
                                @if($errors->has('city')){{ $errors->first('city') }}<br> @endif
                                @if($errors->has('country')){{ $errors->first('country') }}<br> @endif
                                @if($errors->has('postcode')){{ $errors->first('postcode') }}<br> @endif
                                @if($errors->has('email')){{ $errors->first('email') }}<br> @endif
                                @if($errors->has('phone')){{ $errors->first('phone') }}@endif

                            </div>

                        @endif
                        <p class="small hint-text">* Indicates a required field.</p>
                            {{ Form::model($customer, array('route' => array('customers.update', $customer->id), 'method' => 'PUT', 'id' => 'form-work', 'class' => 'form-horizontal', 'role'=>'form', 'autocomplete'=>'off','novalidate'=>'novalidate')) }}{{-- Form model binding to automatically populate our fields with permission data --}}
                            <div class="form-group row{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                <label for="firstname" class="col-md-3 control-label">Firstname *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control error" id="firstname" placeholder="Joan" name="firstname"  value="{{old('firstname',$customer->firstname)}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label for="lastname" class="col-md-3 control-label">Lastname *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control error" id="lastname" placeholder="Smalls" name="lastname"  value="{{old('lastname',$customer->lastname)}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="lastname" class="col-md-3 control-label">Address *</label>
                                <div class="col-md-9{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <textarea class="form-control error" id="address" name="address" required="" style="height: 50px">{{old('address',$customer->address)}}</textarea>
                                   <div class="row">
                                       <div class="col-md-4{{ $errors->has('city') ? ' has-error' : '' }}">
                                           <label for="city" class="control-label">City/Province *</label>

                                           <input type="text" class="form-control error" id="city" placeholder="Bangkok" name="city"  value="{{old('city',$customer->city)}}" required="" aria-required="true" aria-invalid="true">
                                       </div>
                                       <div class="col-md-4{{ $errors->has('country') ? ' has-error' : '' }}">
                                           <label for="country" class="control-label">Country *</label>
                                           <input class="typeahead form-control sample-typehead" type="text" placeholder="Thailand" name="country"   value="{{old('country',$customer->country)}}"  required>
                                       </div>
                                       <div class="col-md-4{{ $errors->has('postcode') ? ' has-error' : '' }}">
                                           <label for="postcode" class="control-label">Postcode *</label>

                                           <input type="text"   oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"  maxlength="5" class="form-control error" id="postcode" placeholder="10700" name="postcode"  value="{{old('postcode',$customer->postcode)}}" required="" aria-required="true" aria-invalid="true">
                                       </div>
                                   </div>
                                </div>

                            </div>
                            <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-3 control-label">Email Address *</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control error" id="email" placeholder="sample@sample.com" name="email"  value="{{old('email',$customer->email)}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="tel" class="col-md-3 control-label">Phone *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control error" id="phone"  name="phone"  value="{{old('phone',$customer->phone)}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>

                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <a href="{{route('customers.index')}}" class="btn btn-hotel btn-primary-hotel">Back</a>
                                    <button class="btn btn-hotel btn-sec-hotel" type="submit">Save Changes</button>
                                </div>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>



        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
@endsection

@section('bottom-scripts')
    <script>
        (function($) {

            'use strict';

            var getBaseURL = function() {
                var url = document.URL;
                return url.substr(0, url.lastIndexOf('/'));
            }

            $(document).ready(function() {


                //Typehead Sample Code

                // Basic Sample using Bloodhound
                // constructs the suggestion engine

                var countries = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: 'http://pages.revox.io/json/countries-list.json'
                });

                // passing in `null` for the `options` arguments will result in the default
                // options being used
                $('.sample-typehead').typeahead(null, {
                    name: 'countries',
                    source: countries
                });



            });

        })(window.jQuery);
    </script>
    <script>
        var inputEl = document.getElementById('phone');
        var goodKey = '0123456789+ ';
        var key = null;

        var checkInputTel = function() {
            var start = this.selectionStart,
                end = this.selectionEnd;

            var filtered = this.value.split('').filter(filterInput);
            this.value = filtered.join("");

            /* Prevents moving the pointer for a bad character */
            var move = (filterInput(String.fromCharCode(key)) || (key == 0 || key == 8)) ? 0 : 1;
            this.setSelectionRange(start - move, end - move);
        }

        var filterInput = function(val) {
            return (goodKey.indexOf(val) > -1);
        }

        /* This function save the character typed */
        var res = function(e) {
            key = (typeof e.which == "number") ? e.which : e.keyCode;
        }

        inputEl.addEventListener('input', checkInputTel);
        inputEl.addEventListener('keypress', res);
    </script>
@endsection



