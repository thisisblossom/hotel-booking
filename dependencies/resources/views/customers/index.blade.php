@extends('layouts.admin')
@section('top-scripts')
    <script type="text/javascript">
        function searchCustomer(str) {
            if (str == "") {
                document.getElementById("data_view").style.display="table-row-group";

                document.getElementById("data_search").style.display="none";

            } else {

                document.getElementById("data_view").style.display="none";
                document.getElementById("paginate").style.display="none";
                document.getElementById("data_search").style.display="table-row-group";
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("data_search").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET","/admin/customers/search?keyword="+str+"&_token=<?php echo csrf_token(); ?>",true);
                xmlhttp.send();
            }
        }
    </script>
    @endsection
@section('mini-menu')
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Customers</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <div class="row">
                    <div class="col-sm-6">
                        <h1>CUSTOMERS
                        </h1>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control btn-top-page pull-right" id="search" placeholder="Search customer.." onkeyup="searchCustomer(this.value);">
                    </div>
                    <div class="col-sm-2">
                        <a href="{{ route('customers.create') }}" class="btn btn-hotel btn-primary-hotel btn-top-page pull-right"><i class="icon-plus"></i> Add Customer</a>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-block">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('flash_message') !!}
                    </div>

                @endif
                @if(Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('warning') !!}
                    </div>

                @endif
                @if(Session::has('danger'))
                    <div class="alert alert-danger" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('danger') !!}
                    </div>

                @endif
                <div class="table-responsive">
                    <div id="basicTable_wrapper" class="dataTables_wrapper no-footer">
                        <form action="{{route('customers_destroymany')}}" method="post">
                            {{csrf_field()}}
                            <table class="table table-hover dataTable no-footer" id="basicTable" role="grid">
                                <thead>
                                <tr role="row">
                                    <th style="width: 1%" class="sorting_desc" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1" aria-label="No.: activate to sort column ascending" aria-sort="descending">No</th>
                                    <th style="width: 41.5%" class="sorting" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1" aria-label="Title: activate to sort column ascending">Name</th>
                                    <th style="width: 41.5%" class="sorting" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1" aria-label="Title: activate to sort column ascending">Email</th>
                                    <th style="width: 15%" class="sorting" tabindex="0" aria-controls="basicTable" rowspan="1" colspan="1">Action</th>
                                    <th style="width:1%" class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="">
                                        <button class="btn btn-link"><i class="icon-trash"></i>
                                        </button>
                                    </th>
                                </tr>

                                </thead>
                                <tbody id="data_view">
                                @if(sizeof($customers) != 0)
                                    @foreach ($customers as $customer)
                                        <tr role="row" class="odd">
                                            <td class="v-align-middle sorting_1"  style="width: 1%">
                                                {{ $customer->id }}
                                            </td>
                                            <td class="v-align-middle" style="width: 41.5%">
                                                {{ $customer->firstname." ".$customer->lastname }}
                                            </td>
                                            <td class="v-align-middle" style="width: 41.5%" >
                                                {{ $customer->email }}
                                            </td>
                                            <td class="v-align-middle" style="width: 15%" >
                                                <a href="{{ URL::to('/admin/customers/'.$customer->id.'/edit') }}" class="btn btn-hotel btn-edit pull-left" style="margin-right: 3px;"><i class="icon-pencil"></i></a>
                                                <button type="button" class="btn btn-hotel btn-delete pull-left" style="margin-right: 3px;" data-target="#deleteModal{{$customer->id}}" data-toggle="modal"><i class="icon-trash"></i></button>
                                                <div class="modal fade fill-in disable-scroll" id="deleteModal{{$customer->id}}" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header clearfix text-left">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body text-center m-t-20">
                                                                <h5>Are you sure you want to delete {{$customer->name}}?</h5>
                                                                <button type="button" class="btn btn-hotel btn-primary-hotel" data-dismiss="modal" aria-hidden="true">Cancel</button>&nbsp;
                                                                <a href="{{ route('customers.destroy', $customer->id) }}" class="btn btn-hotel btn-delete">Delete</a>

                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td class="v-align-middle" style="width:1%">
                                                <div class="checkbox text-center">
                                                    <input type="checkbox" value="{{$customer->id}}" id="checkbox{{$customer->id}}" name="multi_id[]">
                                                    <label for="checkbox{{$customer->id}}" class="no-padding no-margin"></label>
                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach
                                @else
                                    <tr><td style="width:1%"></td><td style="width:41.5%">No customers were found.</td><td style="width:41.5%"></td><td style="width:15%"></td><td style="width:1%"></td></tr>
                                @endif




                                </tbody>
                                <tbody id="data_search" style="display: none">

                                </tbody>
                                <tfoot>
                                <tr><td></td><td></td><td></td><td style="text-align: right">with selected:</td><td><button type="button" class="btn btn-xs btn-hotel btn-delete" data-target="#deleteMany" data-toggle="modal"><i class="icon-trash"></i> Delete</button></td>
                                    <div class="modal fade fill-in disable-scroll" id="deleteMany" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header clearfix text-left">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-center m-t-20">
                                                    <h5>Are you sure you want to delete selected customers ?</h5>
                                                    <button type="button" class="btn btn-hotel btn-primary-hotel" data-dismiss="modal" aria-hidden="true">Cancel</button>&nbsp;
                                                    <button class="btn btn-hotel btn-delete" type="submit">Delete</button>

                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                </tr>



                                </tfoot>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            <div style="text-align: center" id="paginate">
                {{$customers->links()}}
            </div>
        </div>
        <!-- END card -->


        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection