@extends('layouts.admin')

@section('mini-menu')
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
                </a>
                <ul>
                    <li class="active">
                        <a href="{{route('rooms.index')}}"><span class="title">Rooms</span></a>
                    </li>
                    <li>
                        <a href="{{route('room-types.index')}}">Room Types</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Rooms</a></li>
                <li class="breadcrumb-item"><a href="{{route('rooms.index')}}">Rooms</a></li>
                <li class="breadcrumb-item active">{{$type->translateDefault()->name}} : Add Room</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <h1>{{strtoupper($type->translateDefault()->name)}} : ADD ROOM

                </h1>

            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="card card-transparent">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('flash_message'))
                            <div class="alert alert-success" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                {!! Session('flash_message') !!}
                            </div>

                        @endif
                        @if ($errors->has('name') or $errors->has('abbreviation'))
                            <div class="alert alert-danger" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                @if($errors->has('name')){{ $errors->first('name') }}<br>@endif
                                @if($errors->has('abbreviation')){{ $errors->first('abbreviation') }}<br>@endif



                            </div>

                        @endif
                        <p class="small hint-text">* Indicates a required field.</p>
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off" action="{{route('add_room_store',$type->id)}}" method="post" novalidate="novalidate">
                            {{csrf_field()}}
                            <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Name *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control error" id="name" placeholder="A101" name="name"  value="{{old('name')}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>


                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <a href="{{route('rooms.index')}}" class="btn btn-hotel btn-primary-hotel">Back</a>
                                    <button class="btn btn-hotel btn-sec-hotel" type="submit">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
@endsection





