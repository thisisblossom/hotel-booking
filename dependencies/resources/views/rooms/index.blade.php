@extends('layouts.admin')

@section('mini-menu')
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
                </a>
                <ul>
                    <li class="active">
                        <a href="javascript:;"><span class="title">Rooms</span></a>
                    </li>
                    <li>
                        <a href="{{route('room-types.index')}}">Room Types</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Rooms</a></li>
                <li class="breadcrumb-item active">Rooms</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <h1>ROOMS
                </h1>


            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-block">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('flash_message') !!}
                    </div>

                @endif
                @if(Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('warning') !!}
                    </div>

                @endif
                @if(Session::has('danger'))
                    <div class="alert alert-danger" role="alert">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session('danger') !!}
                    </div>

                @endif

            </div>
          @foreach($roomtype as $type)
                <div class="card card-default">
                    <div class="card-header ">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4>{{$type->translateDefault()->name}}</h4>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{route('add_room',$type->id)}}" class="btn btn-hotel btn-primary-hotel pull-right" style="float: right"><i class="icon-plus"></i>  ADD ROOM</a>

                                </div>
                            </div>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-6">
                                <img src="http://via.placeholder.com/320x240" width="100%">

                            </div>
                            <div class="col-lg-6">
                                <?php $room = \App\Room::where('room_type_id',$type->id)->get(); ?>
                                @if(sizeof($room) > 0)
                                        @foreach($room as $room)
                                            <button class="btn btn-success" type="button">{{$room->name}}</button>
                                        @endforeach
                                    @else
                                    <p class="text-center">No room were found.</p>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
              @endforeach
        </div>
        <!-- END card -->

        <!-- DELETE MODAL -->
        @if(isset($rooms))
            @foreach ($rooms as $room)
                <div class="modal fade fill-in disable-scroll" id="deleteModal{{$room->id}}" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                </button>
                            </div>
                            <div class="modal-body text-center m-t-20">
                                <form action="{{ route('room.destroy', $room->id) }}" method="post">
                                    <input type="hidden" name="_method" value="delete" />
                                    {!! csrf_field() !!}
                                    <h5>Are you sure you want to delete room {{ $room->name }}?</h5>
                                    <p>If you delete room {{$room->name}}, This service will be lost in booking</p>
                                    <button type="button" class="btn btn-hotel btn-primary-hotel" data-dismiss="modal" aria-hidden="true">Cancel</button>&nbsp;
                                    <button class="btn btn-hotel btn-delete" type="submit">Delete</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

        @endforeach
    @endif


    <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection

