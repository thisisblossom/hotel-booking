@extends('layouts.admin')

@section('mini-menu')
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
                </a>
                <ul>
                    <li class="active">
                        <a href="{{route('users.index')}}">Users Management</a>
                    </li>

                    <li>
                        <a href="{{route('roles.index')}}"><span class="title">Roles</span></a>
                    </li>

                    <li>
                        <a href="{{route('statuses.index')}}"><span class="title">Default Statuses</span></a>
                    </li>
                    <li>
                        <a href="{{route('languages.index')}}"><span class="title">Languages</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('container')
    <div class="bg-white">
        <div class="container">
            <ol class="breadcrumb breadcrumb-alt">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">System</a></li>
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users Management</a></li>
                <li class="breadcrumb-item active">Edit {{$user->name}}</li>
            </ol>
        </div>
    </div>
    <!-- START JUMBOTRON -->
    <div class="jumbotron">
        <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner heading-padding">
                <!-- START BREADCRUMB -->
                <h1>EDIT {{$user->name}}

                </h1>

            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container    container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="card card-transparent">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('flash_message'))
                            <div class="alert alert-success" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                {!! Session('flash_message') !!}
                            </div>

                        @endif
                        @if (sizeof($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                <button class="close" data-dismiss="alert"></button>
                                @if($errors->has('name')){{ $errors->first('name') }}<br>@endif
                                @if($errors->has('email')){{ $errors->first('email') }}<br>@endif
                                @if($errors->has('password')){{ $errors->first('password') }}<br>@endif
                                @if($errors->has('password_confirmation')){{ $errors->first('password_confirmation') }}<br>@endif
                                @if($errors->has('roles')){{ $errors->first('roles') }}<br>@endif

                            </div>

                        @endif
                        <p class="small hint-text">* Indicates a required field.</p>
                            {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'id' => 'form-work', 'class' => 'form-horizontal', 'role'=>'form', 'autocomplete'=>'off','novalidate'=>'novalidate')) }}{{-- Form model binding to automatically populate our fields with user data --}}
                            {{csrf_field()}}
                            <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Name *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control error" id="name" placeholder="Joan Smalls" name="name"  value="{{old('name', $user->name)}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Email Address *</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control error" id="email" placeholder="sample@sample.com" name="email"  value="{{old('email', $user->email)}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Password *</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control error" id="password" name="password"  value="{{old('password')}}" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Password Confirmation *</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control error" id="password_confirmation" name="password_confirmation"  value="" required="" aria-required="true" aria-invalid="true">
                                </div>
                            </div>
                            <div class="form-group row{{ $errors->has('roles') ? ' has-error' : '' }}">
                                @if(!$roles->isEmpty())

                                    <label class="col-md-3 control-label">Assign Roles *</label>
                                    <div class="col-md-9">
                                        @foreach($user->roles as $user_role)
                                            @foreach ($roles as $role)
                                                @if($user_role->pivot->role_id == $role->id)
                                                    <input name="roles" type="radio" value="{{$role->id}}" checked>
                                                    <label>{{$role->name}}</label><br>
                                                    @else
                                                    <input name="roles" type="radio" value="{{$role->id}}">
                                                    <label>{{$role->name}}</label><br>
                                                @endif
                                            @endforeach
                                        @endforeach


                                    </div>
                                @endif
                            </div>


                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <a href="{{route('users.index')}}" class="btn btn-hotel btn-primary-hotel">Back</a>
                                    <button class="btn btn-hotel btn-sec-hotel" type="submit">Edit</button>
                                </div>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>



        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
@endsection