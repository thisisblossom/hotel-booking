<?php

use Illuminate\Database\Seeder;

class RoomStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new \App\RoomStatus();
        $status->name = "Available";
        $status->save();

        $status = new \App\RoomStatus();
        $status->name = "Not Available";
        $status->save();

        $status = new \App\RoomStatus();
        $status->name = "Booked";
        $status->save();

        $status = new \App\RoomStatus();
        $status->name = "Canceled";
        $status->save();
    }
}
