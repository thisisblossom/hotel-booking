<?php

use Illuminate\Database\Seeder;

class CouponTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coupon_type = new \App\CouponType();
        $coupon_type->name = "Percent";
        $coupon_type->save();

        $coupon_type = new \App\CouponType();
        $coupon_type->name = "Discount";
        $coupon_type->save();

        $coupon_type = new \App\CouponType();
        $coupon_type->name = "New Rates";
        $coupon_type->save();
    }
}
