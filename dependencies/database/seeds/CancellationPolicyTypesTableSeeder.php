<?php

use Illuminate\Database\Seeder;

class CancellationPolicyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $policy_type = new \App\CancellationPolicyType();
        $policy_type->name = "Fully Refundable";
        $policy_type->save();

        $policy_type = new \App\CancellationPolicyType();
        $policy_type->name = "Partially Refundable";
        $policy_type->save();

    }
}
