<?php

use Illuminate\Database\Seeder;
use App\FacilityType;

class FacilityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facility_type = new FacilityType();
        $facility_type->name = "Hotel";
        $facility_type->save();

        $facility_type = new FacilityType();
        $facility_type->name = "Room";
        $facility_type->save();

        $facility_type = new FacilityType();
        $facility_type->name = "Bed";
        $facility_type->save();

    }
}
