<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->unsigned();
            $table->string('main_photo');
            $table->integer('max_adult');
            $table->integer('max_child');
            $table->float('area');
            $table->float('price');
            $table->integer('ordering');
            $table->integer('policy_id')->unsigned();
            $table->timestamps();
            $table->foreign('status_id')->references('id')->on('default_statuses');
            $table->foreign('policy_id')->references('id')->on('cancellation_policies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_types');
    }
}
