<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = "coupons";
    protected $fillable = [
        'name', 'code', 'start_date', 'end_date', 'coupon_type_id', 'status_id', 'max_used','changes'
    ];

    public function type(){
        return $this->belongsTo('App\CouponType','coupon_type_id');
    }
}
