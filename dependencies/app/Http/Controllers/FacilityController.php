<?php

namespace App\Http\Controllers;

use App\DefaultStatus;
use Illuminate\Http\Request;
use App\Facility;
use App\FacilityType;
use App\FacilityTranslation;
use App\Language;
use Validator;

class FacilityController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'manageSetting']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = Facility::orderBy('id','desc')->paginate(10);
        $facility_types = FacilityType::all();
        return view('facilities.index')->with(['facilities'=>$facilities,'facility_types'=>$facility_types]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::orderBy('default','desc')->get();
        $facility_types = FacilityType::all();
        $status = DefaultStatus::all();
        return view('facilities.create')->with(['facility_types'=>$facility_types, 'languages'=>$languages,'status'=>$status]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $language = Language::all();
        $validate = ['facility_type'=>'required', 'status'=>'required'];
        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';

        }
        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $facility = new Facility();
            $facility->facility_type_id = $request->facility_type;
            $facility->status_id = $request->status;
            $facility->save();

            $translation = Facility::find($facility->id);
            $language = Language::all();
            foreach ($language as $language){
               $facility_translation = new FacilityTranslation(['local'=>'en','name'=>$request['name'.$language->abbreviation]]);
                $translation->translationSave()->save($facility_translation);
            }



            return redirect()->route('facilities.index')
                ->with('flash_message',
                    $facility->translateDefault()->name.' added.');




        }
      // return $validate;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/admin/settings/facilities');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::orderBy('default','desc')->get();
        $facility_types = FacilityType::all();
        $facility = Facility::findOrFail($id);
        $status = DefaultStatus::all();
        return view('facilities.edit')->with(['facility_types'=>$facility_types, 'languages'=>$languages, 'facility'=>$facility,'statuses'=>$status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $language = Language::all();
        $validate = ['facility_type'=>'required','status'=>'required'];
        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';

        }
        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $facility = Facility::findOrFail($id);
            $facility->facility_type_id = $request->facility_type;
            $facility->status_id = $request->status;
            $facility->save();

            $language = Language::all();
            foreach ($language as $language){
                $translation = FacilityTranslation::where('facility_id',$id)->where('local', $language->abbreviation)->first();
                $translation->name = $request['name'.$language->abbreviation];
                $translation->save();
            }

            return redirect('/admin/settings/facilities/'.$facility->id.'/edit')
                ->with('flash_message',
                    $facility->translateDefault()->name.' updated.');




        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility_translation = FacilityTranslation::where('facility_id',$id);
        $facility_translation->delete();
        $facility = Facility::findOrFail($id);
        $facility->delete();
        return redirect()->route('facilities.index')
            ->with(['flash_message'=>
                'Facility deleted.']);

    }

    public function destroyMany(Request $request){
        if ($request->multi_id != null){
            $facilities = Facility::findOrFail($request->multi_id);

            foreach ($facilities as $facility){
                $facility_translation = FacilityTranslation::where('facility_id',$facility->id);
                $facility_translation->delete();
                $facility->delete();


            }
            return redirect()->route('facilities.index')
                ->with(['flash_message'=>
                    'Facilities deleted.']);

        }
        else{
            return redirect()->route('facilities.index')
                ->with('warning',
                    'Please select at least one record.');

        }

    }

    public function setFacilityType(Request $request){
       if($request->facility_type != null){
            $facilities = Facility::where('facility_type_id',$request->facility_type)->orderBy('id','desc')->paginate(10);
            $facility_types = FacilityType::all();
            return view('facilities.index')->with(['facilities'=>$facilities,'facility_types'=>$facility_types, 'type'=>$request->facility_type]);

        }
        else{
            return redirect()->route('facilities.index');
        }
    }
}
