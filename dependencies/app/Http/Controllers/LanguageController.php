<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;
use Validator;

class LanguageController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'manageSystem']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $language = Language::orderBy('id','desc')->paginate(10);
        return view('language.index')->with('languages',$language);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('language.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required|unique:languages',
            'abbreviation' =>'required|size:2|unique:languages',

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $language = new Language();
            $input = $request->all();
            $language->fill($input)->save();


            return redirect()->route('languages.index')
                ->with('flash_message',
                    $language->name.' added.');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/admin/system/languages');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $language = Language::findOrFail($id);
        return view('language.edit')->with('language',$language);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required|unique:languages,name,'.$id,
            'abbreviation' =>'required|size:2|unique:languages,abbreviation,'.$id,

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $language = Language::findOrFail($id);
            $input = $request->all();
            $language->fill($input)->save();


            return redirect('/admin/system/languages/'.$language->id.'/edit')
                ->with('flash_message',
                    $language->name . ' language updated.');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $language = Language::findOrFail($id);

        if ($language->default == 1) {
            return redirect()->route('languages.index')
                ->with('danger',
                    'Cannot delete this Language because it is default language.');
        }
        else{
            $language->delete();
            return redirect()->route('languages.index')
                ->with('flash_message',
                    'Language deleted.');
        }
    }

    public function destroyMany(Request $request){
        if ($request->multi_id != null){
            $language = Language::findOrFail($request->multi_id);

            //Make it impossible to delete this specific permission
            $msg = "";
            $count_error=0;
            $count_delete=0;
            foreach ($language as $language){
                if ($language->default == 1) {
                    if($count_error == 0){
                        $msg .= " ".$language->name;
                    }
                    else{
                        $msg .= ", ".$language->name;
                    }
                    $count_error++;

                }
                else{
                    $language->delete();
                    $count_delete++;
                }

            }
            if($count_error != 0 && $count_delete != 0){
                return redirect()->route('languages.index')
                    ->with(['flash_message'=>
                        'Language deleted.','danger'=>'Cannot delete'.$msg." language becuase it is default language."]);
            }
            else if($count_error == 0 && $count_delete != 0){
                return redirect()->route('languages.index')
                    ->with(['flash_message'=>
                        'Language deleted.']);
            }
            else if($count_error != 0 && $count_delete == 0){
                return redirect()->route('languages.index')
                    ->with(['danger'=>'Cannot delete'.$msg." language."]);
            }

        }
        else{
            return redirect()->route('languages.index')
                ->with('warning',
                    'Please select at least one record.');

        }

    }

    public function makeDefault($id){
        $languages = Language::all();
        foreach ($languages as $language){
            $language->default = 0;
            $language->save();
        }
        $language = Language::findOrFail($id);
        $language->default = 1;
        $language->save();
        return redirect()->route('languages.index')
            ->with('flash_message',
                'Default language changed.');

    }
}
