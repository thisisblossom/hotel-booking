<?php

namespace App\Http\Controllers;

use App\DefaultStatus;
use App\Facility;
use App\Language;
use App\Room;
use App\RoomType;
use App\RoomTypeFacility;
use Illuminate\Http\Request;
use Validator;
use App\RoomTypeTranslation;

class RoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room_types = RoomType::orderBy('id','desc')->get();
        return view('room-types.index',['room_types'=>$room_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = DefaultStatus::all();
        $language = Language::all();
        $bed = Facility::where('facility_type_id',3)->get();
        $facility = Facility::where('facility_type_id',2)->get();
        $order = RoomType::max('ordering');
        return view('room-types.create', ['status'=>$status,'languages'=>$language, 'bed'=>$bed, 'order'=>$order+1, 'facilities'=>$facility]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $language = Language::all();
        $validate = ['area' =>'required',
            'max_child' =>'required',
            'max_adult' =>'required',
            'bedtype' =>'required',
            'facilities' =>'required',
            'bathroom' =>'required',
            'status' =>'required',
            'price' =>'required',
            ];

        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';
            $validate['description'.$language->abbreviation] = 'required';

        }
        $validation = Validator::make( $request->all(), $validate
        );
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $ordering = RoomType::max('ordering');
            $room_type = new RoomType();
            $room_type->status_id = $request->status;
            $room_type->bed_type_id = $request->bedtype;
            $room_type->bathroom = $request->bathroom;
            $room_type->max_adult = $request->max_adult;
            $room_type->max_child = $request->max_child;
            $room_type->area = $request->area;
            $room_type->price = $request->price;
            $room_type->ordering = $request->ordering;
            $room_type->save();

            $translation = RoomType::find($room_type->id);
            $language = Language::all();
            foreach ($language as $language){
                $room_type_translation = new RoomTypeTranslation(['local'=>$language->abbreviation,'name'=>$request['name'.$language->abbreviation],'description'=>$request['description'.$language->abbreviation]]);
                $translation->translationSave()->save($room_type_translation);
            }


            for($i = 0 ; $i < sizeof($request->facilities) ; $i++){
                $facility = new RoomTypeFacility();
                $facility->room_Type_id = $room_type->id;
                $facility->facility_id = $request->facilities[$i];
                $facility->save();

            }

            return redirect()->route('room-types.index')
                ->with('flash_message',
                    'Room type added.');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $language = Language::all();
        $validate = ['area' =>'required',
            'max_child' =>'required',
            'max_adult' =>'required',
            'bedtype' =>'required',
            'facilities' =>'required',
            'bathroom' =>'required',
            'status' =>'required',
            'price' =>'required',
        ];

        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';
            $validate['description'.$language->abbreviation] = 'required';

        }
        $validation = Validator::make( $request->all(), $validate
        );
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $room_type = RoomType::where('id',$id);
            $room_type->status_id = $request->status;
            $room_type->bed_type_id = $request->bedtype;
            $room_type->bathroom = $request->bathroom;
            $room_type->max_adult = $request->max_adult;
            $room_type->max_child = $request->max_child;
            $room_type->area = $request->area;
            $room_type->price = $request->price;
            $room_type->ordering = $request->ordering;
            $room_type->save();

            $language = Language::all();
            foreach ($language as $language){
                $room_type_translation = RoomTypeTranslation::where('local', $language->abbreviation)->where('room_type_id',$id)->first();
                $room_type_translation->name = $request['name'.$language->abbreviation];
                $room_type_translation->description = $request['description'.$language->abbreviation];
                $room_type_translation->save();
            }

            $facilities = Facility::where('room_type_id',$id)->get();
            foreach ($facilities as $facilities){
                for($i = 0 ; $i < sizeof($request->facilities) ; $i++){
                    if($request->facilities[$i] != $facilities->id){
                        $facility = new RoomTypeFacility();
                        $facility->room_Type_id = $id;
                        $facility->facility_id = $request->facilities[$i];
                        $facility->save();
                    }

                }

            }

            return redirect()->route('room-types.edit',$id)
                ->with('flash_message',
                    'Room type updated.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::where('room_type_id',$id)->get();
        if(sizeof($room) > 0){
            $room_type_translation = RoomTypeTranslation::where('room_type_id', $id);
            $room_type_translation->delete();
            $room_type_facility = RoomTypeFacility::where('room_type_id',$id);
            $room_type_facility->delete();
            $room_type = RoomType::where('id',$id);
            $room_type->delete();
        }
        return redirect()->route('room-types.index',$id)
            ->with('flash_message',
                'Room type deleted.');

    }

    public function destroyMany(Request $request)
    {
        if ($request->multi_id != null) {
            for($i = 0 ; $i<sizeof($request->multi_id) ; $i++){
                $room = Room::where('room_type_id',$request->multi_id[$i])->get();
                if(sizeof($room) > 0){
                    $room_type_translation = RoomTypeTranslation::where('room_type_id', $request->multi_id[$i]);
                    $room_type_translation->delete();
                    $room_type_facility = RoomTypeFacility::where('room_type_id',$request->multi_id[$i]);
                    $room_type_facility->delete();
                    $room_type = RoomType::where('id',$request->multi_id[$i]);
                    $room_type->delete();
                }

            }

            return redirect()->route('room-types.index',$request->multi_id[$i])
                ->with('flash_message',
                    'Room type deleted.');

        }

    }
}
