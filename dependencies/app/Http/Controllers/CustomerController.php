<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Auth;
use Validator;
use URL;

class CustomerController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'manageCustomer']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('id','desc')->paginate(10); //Get all permissions

        return view('customers.index')->with('customers', $customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'firstname'=>'required',
            'lastname' =>'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'postcode' => 'required|numeric|digits:5',
            'email' => 'required|email|unique:customers',
            'phone' => 'required|numeric',
        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $customer = new Customer();
            $input = $request->all();
            $customer->fill($input)->save();


            return redirect()->route('customers.index')
                ->with('flash_message',
                    ''. $customer->firstname." ".$customer->lastname.' added.');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('customers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customers.edit')->with('customer', $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        // validate the input
        $validation = Validator::make( $request->all(), [
            'firstname'=>'required',
            'lastname' =>'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'postcode' => 'required|numeric|digits:5',
            'email' => 'required|email|unique:customers,email,'.$id,
            'phone' => 'required',
        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $input = $request->all();
            $customer->fill($input)->save();


            return redirect()->route('customers.index')
                ->with('flash_message',
                    ''. $customer->firstname." ".$customer->lastname.' updated.');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();
        return redirect()->route('customers.index')
            ->with('flash_message',
                'Customer deleted.');
    }

    public function destroyMany(Request $request)
    {
        if ($request->multi_id != null){
            $customer = Customer::findOrFail($request->multi_id);

            //Make it impossible to delete this specific permission
            $msg = "";
            $count_error=0;
            $count_delete=0;
            foreach ($customer as $customer){
                    $customer->delete();
                    $count_delete++;
                }


            if($count_error != 0 && $count_delete != 0){
                return redirect()->route('customers.index')
                    ->with(['flash_message'=>
                        'Customers deleted.','danger'=>'Cannot delete'.$msg."."]);
            }
            else if($count_error == 0 && $count_delete != 0){
                return redirect()->route('customers.index')
                    ->with(['flash_message'=>
                        'Customers deleted.']);
            }
            else if($count_error != 0 && $count_delete == 0){
                return redirect()->route('customers.index')
                    ->with(['danger'=>'Cannot delete'.$msg."."]);
            }

        }
        else{
            return redirect()->route('customers.index')
                ->with('warning',
                    'Please select at least one record.');

        }

    }
    public function search(Request $request){
        $customer = Customer::whereRaw("concat(firstname,' ',lastname) like '%$request->keyword%' or email like '%$request->keyword%'")->first();
        $result = null;
        if($customer != null){
            $result .= "<tr role='row' class='odd'>
                                            <td class='v-align-middle sorting_1' style='width:1%'>
                                                $customer->id
                                            </td>
                                            <td class='v-align-middle' style='width:41.5%'>
                                                $customer->firstname $customer->lastname
                                            </td>
                                            <td class='v-align-middle'  style='width:41.5%'>
                                                $customer->email
                                            </td>
                                            <td class='v-align-middle'  style='width:15%'>
                                                <a href='".URL::to("admin/customers/$customer->id/edit")."' class=\"btn btn-hotel btn-edit pull-left\" style='margin-right: 3px;'><i class='icon-pencil'></i></a>
                                                <button type='button' class='btn btn-hotel btn-delete pull-left' style='margin-right: 3px;' data-target='#deleteModal$customer->id' data-toggle='modal'><i class='icon-trash'></i></button>
                                                <div class='modal fade fill-in disable-scroll' id='deleteModal$customer->id' tabindex='-1' role='dialog' style='display: none;' aria-hidden='true'>
                                                    <div class='modal-dialog'>
                                                        <div class='modal-content'>
                                                            <div class='modal-header clearfix text-left'>
                                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='pg-close fs-14'></i>
                                                                </button>
                                                            </div>
                                                            <div class='modal-body text-center m-t-20'>
                                                                <h5>Are you sure you want to delete $customer->name?</h5>
                                                                <button type='button' class='btn btn-hotel btn-primary-hotel' data-dismiss='modal' aria-hidden='true'>Cancel</button>&nbsp;
                                                                <a href='".route('customers.destroy', $customer->id)."' class='btn btn-hotel btn-delete'>Delete</a>

                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td class='v-align-middle'  style='width:1%'>
                                                <div class='checkbox text-center'>
                                                    <input type='checkbox' value='$customer->id' id='checkbox$customer->id' name='multi_id[]'>
                                                    <label for='checkbox$customer->id' class='no-padding no-margin'></label>
                                                </div>
                                            </td>
                                        </tr>";
        }else{
            $result .= "<tr><td  style='width:1%'></td><td  style='width:41.5%'>No customers were found.</td><td  style='width:41.5%'></td><td  style='width:15%'></td><td style='width:1%'></td></tr>";
        }
        return $result;

    }
}
