<?php

namespace App\Http\Controllers;

use App\DefaultStatus;
use App\Language;
use App\Service;
use App\ServiceTranslation;
use Illuminate\Http\Request;
use Validator;

class ServiceController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'manageSetting']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('id','desc')->paginate(10);
        return view('services.index',['services'=>$services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = DefaultStatus::all();
        $language = Language::all();
        return view('services.create',['status'=>$status, 'languages'=>$language]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $language = Language::all();
        $validate = [ 'status'=>'required'];
        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';

        }
        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $service = new Service();
            $service->status_id = $request->status;
            $service->save();

            $translation = Service::find($service->id);
            $language = Language::all();
            foreach ($language as $language){
                $service_translation = new ServiceTranslation(['local'=>'en','name'=>$request['name'.$language->abbreviation]]);
                $translation->translationSave()->save($service_translation);
            }



            return redirect()->route('services.index')
                ->with('flash_message',
                    $service->translateDefault()->name.' service added.');




        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin/settings/services');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        $status = DefaultStatus::all();
        $languages = Language::all();
        return view('services.edit')->with(['service'=>$service, 'languages'=>$languages, 'status'=>$status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $language = Language::all();
        $validate = [ 'status'=>'required'];
        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';

        }
        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $service = Service::findOrFail($id);
            $service->status_id = $request->status;
            $service->save();

            $language = Language::all();
            foreach ($language as $language){
                $translation = ServiceTranslation::where('service_id',$id)->where('local', $language->abbreviation)->first();
                $translation->name = $request['name'.$language->abbreviation];
                $translation->save();
            }

            return redirect()->route('services.edit', $service->id)
                ->with('flash_message',
                    $service->translateDefault()->name.' service updated.');




        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service_translation = ServiceTranslation::where('service_id',$id);
        $service_translation->delete();
        $service = Service::findOrFail($id);
        $service->delete();

        return redirect()->route('services.index', $service->id)
            ->with('flash_message',
               'Service deleted.');
    }

    public function destroyMany(Request $request)
    {
        if ($request->multi_id != null){
            $services = Service::findOrFail($request->multi_id);

            foreach ($services as $service){
                $service_translation = ServiceTranslation::where('service_id',$service->id);
                $service_translation->delete();
                $service->delete();


            }
            return redirect()->route('services.index')
                ->with(['flash_message'=>
                    'Services deleted.']);

        }
        else{
            return redirect()->route('services.index')
                ->with('warning',
                    'Please select at least one record.');

        }
    }
}
