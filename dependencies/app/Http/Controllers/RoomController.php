<?php

namespace App\Http\Controllers;

use App\Room;
use App\RoomStatus;
use App\RoomType;
use Illuminate\Http\Request;
use Validator;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roomtype = RoomType::where('status_id',1)->get();
        return view('rooms.index',['roomtype'=>$roomtype]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $type = RoomType::findOrFail($id);
        return view('rooms.create')->with(['type'=>$type]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required|unique:rooms'

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $room = new Room();
            $room->room_status_id = 1;
            $room->name = $request->name;
            $room->room_type_id = $id;
            $room->save();


            return redirect()->route('rooms.index')
                ->with('flash_message', 'Room added.');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room_status = RoomStatus::all();
        $room = Room::findOrFail($id);
        return view('rooms.edit',['status'=>$room_status, 'room'=>$room]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required|unique:rooms,name,'.$id,
            'status'=>'required'

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $room = Room::findOrFail($id);
            $room->room_status_id = $request->status;
            $room->name = $request->name;
            $room->room_type_id = $id;
            $room->save();


            return redirect()->route('rooms.edit',$id)
                ->with('flash_message', 'Room updated.');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::findOrFail($id);
        $room->delete();

        return redirect()->route('rooms.index',$id)
            ->with('flash_message', 'Room updated.');
    }
}
