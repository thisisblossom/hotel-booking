<?php

namespace App\Http\Controllers;

use App\CancellationPolicyType;
use App\DefaultStatus;
use App\Language;
use App\Policy;
use App\PolicyTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Validator;

class PolicyController extends Controller
{

    public function __construct() {
        $this->middleware(['auth', 'manageSetting']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $policy = Policy::orderBy('id', 'desc')->paginate(10);
        return view('policies.index')->with(['policies'=>$policy]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = DefaultStatus::all();
        $language = Language::all();
        $type = CancellationPolicyType::all();
        return view('policies.create')->with(['status'=>$status, 'languages'=>$language, 'type'=>$type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $language = Language::all();
        $validate = [ 'status'=>'required'];
        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';

        }
        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $policy = new Policy();
            $policy->status_id = $request->status;
            $policy->save();

            $translation = Policy::findOrFail($policy->id);
            $language = Language::all();
            foreach ($language as $language) {
                $policy_translation = new PolicyTranslation(['local' => 'en', 'name' => $request['name' . $language->abbreviation], 'description' => $request['description' . $language->abbreviation]]);
                $translation->translationSave()->save($policy_translation);
            }


            return redirect()->route('policy.index')
                ->with('flash_message',
                    $policy->translateDefault()->name . ' added.');
        }

        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin/settings/policy');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $policy = Policy::findOrFail($id);
        $status = DefaultStatus::all();
        $language = Language::all();
        return view('policies.edit')->with(['status'=>$status, 'languages'=>$language, 'policy'=>$policy]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $language = Language::all();
        $validate = [ 'status'=>'required'];
        foreach($language as $language){
            $validate['name'.$language->abbreviation] = 'required';
            $validate['description'.$language->abbreviation] = 'required';

        }
        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

        // redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $policy = Policy::findOrFail($id);
            $policy->status_id = $request->status;
            $policy->save();

            $language = Language::all();
            foreach ($language as $language){
                $policy_translation = PolicyTranslation::where('local', $language->abbreviation)->where('policy_id', $id)->first();
                $policy_translation->name = $request['name'.$language->abbreviation];
                $policy_translation->description = $request['description'.$language->abbreviation];
                $policy_translation->save();
            }

            return redirect()->route('policy.edit', $id)
                ->with('flash_message',
                    $policy->translateDefault()->name . ' updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $policy_translation = PolicyTranslation::where('policy_id',$id);
        $policy_translation->delete();
        $policy = Policy::findOrFail($id);
        $policy->delete();
        return redirect()->route('policy.index')
            ->with(['flash_message'=>
                'Policy deleted.']);
    }

    public function destroyMany(Request $request){
        if ($request->multi_id != null){
            $policy = Policy::findOrFail($request->multi_id);

            foreach ($policy as $policy){
                $policy_translation = PolicyTranslation::where('policy_id',$policy->id);
                $policy_translation->delete();
                $policy->delete();


            }
            return redirect()->route('policy.index')
                ->with(['flash_message'=>
                    'Policy deleted.']);

        }
        else{
            return redirect()->route('policy.index')
                ->with('warning',
                    'Please select at least one record.');

        }

    }

    public function saveRule(Request $request){

        $validate = [ 'percent'=>'required', 'days'=>'required'];

        // validate the input
        $validation = Validator::make( $request->all(),$validate
        );

        // redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }else{

            Session::put('rule_percent',[$request->percent]);
            Session::put('rule_days',[$request->days]);
            return Redirect::back();

        }

    }

}
