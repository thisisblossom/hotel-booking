<?php

namespace App\Http\Controllers;

use App\Facility;
use Illuminate\Http\Request;
use App\FacilityType;
use Validator;

class FacilityTypeController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'manageSetting']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facility_type = FacilityType::orderBy('id', 'desc')->paginate(10);
        return view('facility_types.index')->with('facility_types', $facility_type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('facility_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required|unique:facility_types',

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $facility_type = new FacilityType();
            $input = $request->all();
            $facility_type->fill($input)->save();


            return redirect()->route('facility-types.index')
                ->with('flash_message',
                    $facility_type->name.' added.');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/admin/settings/facility-types');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility_type = FacilityType::findOrFail($id);
        return view('facility_types.edit')->with('facility_type', $facility_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required|unique:facility_types,name,'.$id,

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $facility_type = FacilityType::findOrFail($id);
            $input = $request->all();
            $facility_type->fill($input)->save();

            return redirect('/admin/settings/facility-types/'.$facility_type->id.'/edit')
                ->with('flash_message',
                    $facility_type->name.' updated.');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility_type = FacilityType::findOrFail($id);
        $facility = Facility::where('facility_type_id', $facility_type->id)->get();

        //Make it impossible to delete this specific permission
       if (sizeof($facility) != 0) {
            return redirect()->route('facility-types.index')
                ->with('danger',
                    'Cannot delete this facility type.');
        }
        else{
           if($facility_type->id == 1 or $facility_type->id == 2){
               return redirect()->route('facility-types.index')
                   ->with('danger',
                       'Cannot delete this facility type because it a system default. But you can edit facility type name.');

           }
           else{
               $facility_type->delete();
               return redirect()->route('facility-types.index')
                   ->with('flash_message',
                       'Facility type deleted.');
           }
        }
    }

    public function destroyMany(Request $request){
        if ($request->multi_id != null){
            $facility_types = FacilityType::findOrFail($request->multi_id);
            //Make it impossible to delete this specific permission
            $msg = "";
            $count_error=0;
            $count_delete=0;
            foreach ($facility_types as $facility_type){
                $facility = Facility::where('facility_type_id', $facility_type->id)->get();
                if (sizeof($facility) != 0) {
                    if($count_error == 0){
                        $msg .= " ".$facility_type->name;
                    }
                    else{
                        $msg .= ", ".$facility_type->name;
                    }
                    $count_error++;

                }
                elseif($facility_type->id == 1 or $facility_type->id == 2){
                    if($count_error == 0){
                        $msg .= " ".$facility_type->name;
                    }
                    else{
                        $msg .= ", ".$facility_type->name;
                    }
                    $count_error++;

                }
                else{
                    $facility_type->delete();
                    $count_delete++;
                }

            }
            if($count_error != 0 && $count_delete != 0){
                return redirect()->route('facility-types.index')
                    ->with(['flash_message'=>
                        'Facility types deleted.','danger'=>'Cannot delete facility type'.$msg."."]);
            }
            else if($count_error == 0 && $count_delete != 0){
                return redirect()->route('facility-types.index')
                    ->with(['flash_message'=>
                        'Facility types deleted.']);
            }
            else if($count_error != 0 && $count_delete == 0){
                return redirect()->route('facility-types.index')
                    ->with(['danger'=>'Cannot delete facility type'.$msg.". Because it is a system default or it being used."]);
            }

        }
        else{
            return redirect()->route('facility-types.index')
                ->with('warning',
                    'Please select at least one record.');

        }

    }
}
