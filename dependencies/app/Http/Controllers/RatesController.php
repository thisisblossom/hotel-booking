<?php

namespace App\Http\Controllers;

use App\Rates;
use App\RatesDays;
use App\Room;
use Illuminate\Http\Request;
use App\RoomType;

class RatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roomtype = RoomType::where('status_id',1)->get();
        return view('rates.index',['roomtype'=>$roomtype]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $room_type = RoomType::findOrFail($id);
        return view('rates.create', ['type'=>$id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'change'=>'required',
            'days'=>'required',

        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $price = RoomType::where('id',$id)->first();
            $rate = new Rates();
            $rate->name = $request->name;
            $rate->start_date = $request->start_date;
            $rate->end_date = $request->end_date;
            $rate->changes = $request->changes;
            $rate->new_price = $price * ($request->changes/100);
            $rate->save();

            for($i = 0 ;$i<sizeof($request->days); $i++){
                $rate_day = new RatesDays();
                $rate->rate_id = $rate->id;
                $rate->day = $request->days[$i];
                $rate_day->save();
            }


            return redirect()->route('rates.index')
                ->with('flash_message',
                    'Rate rule added.');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rate = Rates::findOrFail($id);
        $days = RatesDays::where('rate_id', $id)->get();
        return view('rates.edit', ['rate'=>$rate, 'day'=>$days]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $room_type_id)
    {
        // validate the input
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'change' => 'required',
            'days' => 'required',

        ]);

// redirect on validation error
        if ($validation->fails()) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors($validation->messages());
        } else {
            $price = RoomType::where('id', $room_type_id)->first();
            $rate = Rates::findOrFail($id);
            $rate->name = $request->name;
            $rate->start_date = $request->start_date;
            $rate->end_date = $request->end_date;
            $rate->changes = $request->changes;
            $rate->new_price = $price * ($request->changes / 100);
            $rate->save();

            $rate_day = RatesDays::where('rate_id', $id)->get();
            foreach ($rate_day as $rate_day){
                for ($i = 0; $i < sizeof($request->days); $i++) {
                    if($rate_day->day == $request->days[$i]){
                        $rate_day = new RatesDays();
                        $rate->rate_id = $rate->id;
                        $rate->day = $request->days[$i];
                        $rate_day->save();
                    }
                }
            }



            return redirect()->route('rates.edit',$id)
                ->with('flash_message',
                    'Rate rule updated.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rate = RatesDays::where('rate_id',$id);
        $rate->delete();
        $rate = Rates::findOrFail($id);
        $rate->delete();

        return redirect()->route('rates.index')
            ->with('flash_message',
                'Rate rule deleted.');
    }
}
