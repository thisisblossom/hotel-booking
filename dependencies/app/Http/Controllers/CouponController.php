<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\CouponRoomType;
use App\DefaultStatus;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupon = Coupon::orderBy('id','desc')->get();
        return view('coupon.index', ['coupon'=>$coupon]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = DefaultStatus::all();
        return view('coupon.create', ['status'=>$status]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required',
            'code' =>'required',
            'type' => 'required',
            'status' => 'required',
            'max_used' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'room_type'=>'required',
            'changes'=>'required'
        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $coupon = new Coupon();
            $coupon->name = $request->name;
            $coupon->code = $request->code;
            $coupon->coupon_type_id = $request->type;
            $coupon->status_id = $request->status;
            $coupon->start_date = $request->start_date;
            $coupon->end_date = $request->end_date;
            $coupon->max_used = $request->max_used;
            $coupon->changes = $request->changes;
            $coupon->save();

            for($i = 0 ; $i < sizeof($request->room_type) ; $i++){
                $coupon_room = new CouponRoomType();
                $coupon_room->coupon_id = $coupon->id;
                $coupon_room->room_type_id = $request->room_type[$i];
                $coupon_room->save();
            }


            return redirect()->route('customers.index')
                ->with('flash_message',
                    'Coupon added.');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = DefaultStatus::all();
        $coupon = Coupon::findOrFail($id);
        return view('coupon.edit', ['coupon'=>$coupon, 'status'=>$status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the input
        $validation = Validator::make( $request->all(), [
            'name'=>'required',
            'code' =>'required',
            'type' => 'required',
            'status' => 'required',
            'max_used' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'room_type'=>'required',
            'changes'=>'required'
        ]);

// redirect on validation error
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        else {
            $coupon =Coupon::findOrFail($id);
            $coupon->name = $request->name;
            $coupon->code = $request->code;
            $coupon->coupon_type_id = $request->type;
            $coupon->status_id = $request->status;
            $coupon->start_date = $request->start_date;
            $coupon->end_date = $request->end_date;
            $coupon->max_used = $request->max_used;
            $coupon->changes = $request->changes;
            $coupon->save();

            $coupon_room = CouponRoomType::where('coupon_id',$id)->get();
            foreach ($coupon_room as $coupon_room){
                for($i = 0 ; $i < sizeof($request->room_type) ; $i++){
                   if($coupon_room->room_type_id != $request->room_type[$i]){
                       $coupon_rooms = new CouponRoomType();
                       $coupon_rooms->coupon_id = $coupon->id;
                       $coupon_rooms->room_type_id = $request->room_type[$i];
                       $coupon_rooms->save();
                   }
                }
            }
            return redirect()->route('coupon.edit',$id)
                ->with('flash_message',
                    'Coupon updated.');
        }




        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon_room = CouponRoomType::where('coupon_id',$id);
        $coupon_room->delete();
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();

        return redirect()->route('coupon.index')
            ->with('flash_message',
                'Coupon deleted.');
    }
}
