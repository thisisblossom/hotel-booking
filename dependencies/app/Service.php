<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    protected $fillable = ['status_id'];

    public function translation(){
        return $this->hasMany('App\ServiceTranslation');
    }

    public function translate($local){
        return $this->hasMany('App\ServiceTranslation','service_id')->where('local',$local)->first();
    }

    public function translateDefault(){
        $language = Language::where('default',1)->first();
        return $this->hasMany('App\ServiceTranslation','service_id')->where('local',$language->abbreviation)->first();
    }

    public function translationSave(){
        return $this->hasMany('App\ServiceTranslation','service_id');
    }

    public function status(){
        return $this->belongsTo('App\DefaultStatus','status_id');
    }


}
