<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingSetting extends Model
{
    protected $table = "booking_settings";
    protected $fillable = [
        'pre_booking', 'booking_length', 'check_in', 'check_out'
    ];
}
