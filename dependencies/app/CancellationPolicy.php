<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancellationPolicy extends Model
{
    protected $table = "cancellation_policies";
    protected $fillable = [
        'policy_type_id', 'description'

    ];
    public function type(){
        return $this->belongsTo('App\CancellationPolicyType','policy_type_id');
    }
}
