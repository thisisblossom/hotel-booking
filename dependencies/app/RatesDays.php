<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatesDays extends Model
{
    protected $table = "rates_days";
    protected $fillable = [
        'rate_id', 'day'

    ];
    public function rates(){
        return $this->belongsTo('App\Rates','rate_id');
    }
}
