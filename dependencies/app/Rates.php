<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rates extends Model
{
    protected $table = "rates";
    protected $fillable = [
        'room_type_id', 'name', 'start_date', 'end_date', 'change', 'new_price'
    ];
    public function roomType(){
        return $this->belongsTo('App\RoomType','room_type_id');
    }
}
