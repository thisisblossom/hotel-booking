<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomTypePhoto extends Model
{
    protected $table = "room_type_photos";
    protected $fillable = [
        'room_type_id','photo', 'ordering'
    ];
}
