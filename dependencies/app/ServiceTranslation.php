<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTranslation extends Model
{
    protected $table = "service_translations";
    protected $fillable = [
        'service_id', 'local', 'name'
    ];
}
