<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = "policies";
    protected $fillable = ['status_id'];

    public function translate($local){
        return $this->hasMany('App\PolicyTranslation','policy_id')->where('local',$local)->first();
    }

    public function translateDefault(){
        $language = Language::where('default',1)->first();
        return $this->hasMany('App\PolicyTranslation','policy_id')->where('local',$language->abbreviation)->first();
    }

    public function translationSave(){
        return $this->hasMany('App\PolicyTranslation','policy_id');
    }

    public function status(){
        return $this->belongsTo('App\DefaultStatus','status_id');
    }

}
