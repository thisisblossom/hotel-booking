<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Facility extends Model
{
    protected $table = "facilities";
    protected $fillable = ['facility_type_id','status_id'];

    public function translate($local){
        return $this->hasMany('App\FacilityTranslation','facility_id')->where('local',$local)->first();
    }

    public function translateDefault(){
        $language = Language::where('default',1)->first();
        return $this->hasMany('App\FacilityTranslation','facility_id')->where('local',$language->abbreviation)->first();
    }

    public function translationSave(){
        return $this->hasMany('App\FacilityTranslation','facility_id');
    }


    public function facilityType(){
        return $this->belongsTo('App\FacilityType');
    }

    public function status(){
        return $this->belongsTo('App\DefaultStatus','status_id');
    }
}
