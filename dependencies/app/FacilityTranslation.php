<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityTranslation extends Model
{
    protected $table = "facility_translations";
    protected $fillable = [
        'facility_id', 'local', 'name'
    ];
}
