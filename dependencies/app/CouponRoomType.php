<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponRoomType extends Model
{
    protected $table = "coupon_room_types";
    protected $fillable = [
        'room_type_id', 'coupon_id'

    ];
}
