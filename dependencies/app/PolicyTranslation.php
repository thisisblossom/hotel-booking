<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyTranslation extends Model
{
    protected $table = "policy_translations";
    protected $fillable = [
        'policy_id', 'local', 'name', 'description'
    ];
}
