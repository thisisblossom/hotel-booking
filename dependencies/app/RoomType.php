<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $table = "room_types";
    protected $fillable = [
        'status_id', 'main_photo', 'max_adult', 'max_child', 'ordering', 'price', 'area'
    ];

    public function translate($local){
        return $this->hasMany('App\RoomTypeTranslation','room_type_id')->where('local',$local)->first();
    }

    public function translateDefault(){
        $language = Language::where('default',1)->first();
        return $this->hasMany('App\RoomTypeTranslation','room_type_id')->where('local',$language->abbreviation)->first();
    }

    public function translationSave(){
        return $this->hasMany('App\RoomTypeTranslation','room_type_id');
    }

    public function status(){
        return $this->belongsTo('App\DefaultStatus','status_id');
    }


}
