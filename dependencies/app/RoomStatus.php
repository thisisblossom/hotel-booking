<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomStatus extends Model
{
    protected $table = "room_statuses";
    protected $fillable = [
        'name'
    ];
}
