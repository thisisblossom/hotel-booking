<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomTypeTranslation extends Model
{
    protected $table = "room_type_translations";
    protected $fillable = [
      'local', 'name', 'description'
    ];
}
