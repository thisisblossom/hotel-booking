<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancellationPolicyType extends Model
{
    protected $table = "cancellation_policy_types";
    protected $fillable = [
        'name'
    ];
}
