<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomTypeFacility extends Model
{
    protected $table = "room_type_facilities";
    protected $fillable = [
      'room_type_id','facility_id'
    ];


}
