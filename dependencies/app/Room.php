<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = "rooms";
    protected $fillable = [
        'name', 'room_status_id', 'room_type_id'
    ];


    public function roomType(){
        return $this->belongsTo('App\RoomType','room_type_id');
    }

    public function status(){
        return $this->belongsTo('App\RoomStatus','status_id');
    }
}
